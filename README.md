REQUIRE 
-------------
- maven
- java 8

HOW TO BUILD
----------------
run 'mvn clean install' on root project


HOW TO RUN 
----------------
After build go to /target and run RegisterService.jar with command 'java -jar RegisterService.jar'  
Access URL is 'localhost:8080' please check APISpec.txt for more information about API