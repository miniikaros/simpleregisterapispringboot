package me.test.service.register.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import me.test.service.register.entity.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
	
	@Query("SELECT u FROM User u WHERE u.username = ?1")
	User findUserByUsername(String username);
	
	@Query("SELECT u.hashPassword FROM User u WHERE u.username = ?1")
	String findUserPasswordByUsername(String username);
	
	@Query("SELECT case when count(u)> 0 then true else false end FROM User u WHERE u.username = ?1")
	boolean isUserExist(String username);

}
