package me.test.service.register.entity;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.NaturalId;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Getter;
import me.test.service.register.constant.MEMBER_TYPE;

@Data
@Entity
public class User {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@NaturalId
	private String username;

	@Getter(value = AccessLevel.NONE)
	private String hashPassword;
	
	@JsonIgnore
	public String getHashPassword() {
		return hashPassword;
	}
	
	private String refCode;

	@Enumerated(EnumType.STRING)
	private MEMBER_TYPE type;
	
	@Getter(value = AccessLevel.NONE)
	private String encryptedSalary;
	@JsonIgnore
	public String getEncryptedSalary() {
		return encryptedSalary;
	}
	
	private String firstname;
	private String lastname;
	private String mobilePhone;
	private String address;

}
