package me.test.service.register.service;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import me.test.service.register.component.ConfidentialConverter;
import me.test.service.register.component.PasswordEncoderComponent;
import me.test.service.register.constant.API_ERROR_STATUS;
import me.test.service.register.constant.API_RESULT;
import me.test.service.register.entity.User;
import me.test.service.register.model.ConfidentialData;
import me.test.service.register.model.LoginData;
import me.test.service.register.model.RegisterData;
import me.test.service.register.model.RegisterResult;
import me.test.service.register.model.UserData;
import me.test.service.register.repository.UserRepository;

@Service
public class RegisterService {
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private PasswordEncoderComponent passwordHashComponent;
	
	@Autowired
	private ConfidentialConverter confidentialConverter;

	@Transactional
	public void saveUser(RegisterResult registerResult, RegisterData registerData) {
		boolean isExist = userRepository.isUserExist(registerData.getLoginData().getUsername());
		
		if(isExist) {
			registerResult.setResult(API_RESULT.REJECTED);
			registerResult.setError(API_ERROR_STATUS.DUPLICATE_DATA);
			return;
		}
		
		User user = buildUser(registerData);
		userRepository.save(user);
		
		registerResult.setResult(API_RESULT.COMPLETED);
	}

	private User buildUser(RegisterData registerData) {
		LoginData loginData = registerData.getLoginData();
		UserData userData = registerData.getUserData();
		ConfidentialData confidentialData = registerData.getConfidentialData();
		
		User user = new User();
		user.setRefCode(registerData.getReference());
		
		
		user.setUsername(loginData.getUsername());
		user.setHashPassword(passwordHashComponent.hashPassword(loginData.getPassword()));
		
		
		user.setFirstname(userData.getFirstname());
		user.setLastname(userData.getLastname());
		user.setMobilePhone(userData.getMobilePhone());
		user.setAddress(userData.getAddress());
		
		user.setType(userData.getMemberType());
		String encryptedSalary = confidentialConverter.encrypt(confidentialData.getSalary().toString());
		user.setEncryptedSalary(encryptedSalary);
		
		return user;
	}

}
