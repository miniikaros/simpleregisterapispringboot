package me.test.service.register.service;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import me.test.service.register.entity.User;
import me.test.service.register.model.LoginData;
import me.test.service.register.repository.UserRepository;

@Service
public class AdminService {

	@Autowired
	private UserRepository userRepository;
	
	@Transactional
	public User findUser(LoginData loginData) {
		return userRepository.findUserByUsername(loginData.getUsername());
	}
}
