package me.test.service.register.model;

import java.time.LocalDate;

import lombok.Data;

@Data
public class RegisterData {
	private UserData userData;
	private LoginData loginData;
	private ConfidentialData confidentialData;
	private LocalDate registerDate;
	private String reference;
}
