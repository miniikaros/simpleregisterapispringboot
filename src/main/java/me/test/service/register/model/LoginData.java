package me.test.service.register.model;

import lombok.Data;

@Data
public class LoginData {
	private String username;
	private String password;
	
	public LoginData() {}
	public LoginData(String username, String password) {
		this.username=username;
		this.password=password;
	}
}
