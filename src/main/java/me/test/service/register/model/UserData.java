package me.test.service.register.model;

import java.io.Serializable;

import lombok.Data;
import me.test.service.register.constant.MEMBER_TYPE;

@Data
public class UserData implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6027656717386821025L;
	
	private String firstname;
	private String lastname;
	private String address;
	private String mobilePhone;
	private MEMBER_TYPE memberType;
	
}
