package me.test.service.register.model;

import java.io.Serializable;

import lombok.Data;

@Data
public class ConfidentialData implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 6252154919691506099L;
	private Double salary;
}
