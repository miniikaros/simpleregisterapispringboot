package me.test.service.register.model;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;
import me.test.service.register.constant.API_ERROR_STATUS;
import me.test.service.register.constant.API_RESULT;
import me.test.service.register.constant.REGISTER_FIELD_ERROR;

@Data
@JsonInclude (value = Include.NON_NULL)
public class RegisterResult implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private List<REGISTER_FIELD_ERROR> validationError;
	private API_ERROR_STATUS error;
	private API_RESULT result;

}
