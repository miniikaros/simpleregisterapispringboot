package me.test.service.register.controller;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;

import me.test.service.register.component.RegisterComponent;
import me.test.service.register.constant.API_ERROR_STATUS;
import me.test.service.register.constant.API_RESULT;
import me.test.service.register.constant.MEMBER_TYPE;
import me.test.service.register.constant.REGISTER_FIELD_ERROR;
import me.test.service.register.model.ConfidentialData;
import me.test.service.register.model.LoginData;
import me.test.service.register.model.RegisterData;
import me.test.service.register.model.RegisterResult;
import me.test.service.register.model.UserData;
import me.test.service.register.service.RegisterService;


@RequestMapping("/api/public/register")
@RestController
public class RegisterController {
	
	@Autowired
	private RegisterComponent registerComponent;
	
	@Autowired
	private RegisterService registerService;

	@Autowired
	private ObjectMapper objectMapper;
	
	@PostMapping(path = "/newuser",produces = {MediaType.APPLICATION_JSON_VALUE} )
	public ResponseEntity<RegisterResult> register(@RequestBody String jsonStr){
		RegisterResult rs = new RegisterResult();
		RegisterData registerData = null;
		try {
			registerData = objectMapper.readValue(jsonStr, RegisterData.class);
		} catch (Exception e) {
			rs.setResult(API_RESULT.FAIL);
			rs.setError(API_ERROR_STATUS.INVALID_DATA_FORMAT);
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(rs);
		} 
		registerData.setRegisterDate(LocalDate.now());
		
		List<REGISTER_FIELD_ERROR> errList = registerComponent.validateData(registerData);
		if(errList.size() > 0) {
			rs.setResult(API_RESULT.FAIL);
			rs.setValidationError(errList);
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(rs);
		}
		
		String refCode = registerComponent.getReferenceCode(registerData.getRegisterDate(),
				registerData.getUserData().getMobilePhone());
		registerData.setReference(refCode);
		
		MEMBER_TYPE memberType = registerComponent.calculateMemberType(registerData.getConfidentialData().getSalary());
		
		if(memberType == null) {
			rs.setResult(API_RESULT.REJECTED);
			return ResponseEntity.ok(rs);
		}
		
		registerData.getUserData().setMemberType(memberType);
		
		registerService.saveUser(rs, registerData);
		
		return ResponseEntity.ok(rs);
		
	}
	
	@GetMapping(path = "/mock/req/newuser",produces = {MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<Object> mockReqRegister(){
		RegisterData data = new RegisterData();
		data.setConfidentialData(new ConfidentialData());
		data.setLoginData(new LoginData());
		data.setUserData(new UserData());
		data.setRegisterDate(LocalDate.now());
		data.getLoginData().setUsername("Meena");
		data.getLoginData().setPassword("P@ssw0rd");
		data.getUserData().setFirstname("มีนา");
		data.getUserData().setLastname("รักดี");
		data.getUserData().setMobilePhone("0919871234");
		data.getUserData().setAddress("123/4 siam 10500");
		data.getConfidentialData().setSalary(99999.00);
		return ResponseEntity.ok(data);
	}
}
