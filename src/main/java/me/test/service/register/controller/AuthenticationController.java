package me.test.service.register.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import me.test.service.register.config.security.AuthenticationService;
import me.test.service.register.model.AuthResult;

@RestController
public class AuthenticationController {

	@Autowired
	AuthenticationService authenticationService;
	
	@PostMapping(path = "/api/authenticate")
	public ResponseEntity<AuthResult> authenticate(@RequestParam("username") String username,
			@RequestParam("password") String password) {
		
		AuthResult authResult = new AuthResult();
		authResult.setStatus("success");

		return ResponseEntity.ok(authResult);

	}
}
