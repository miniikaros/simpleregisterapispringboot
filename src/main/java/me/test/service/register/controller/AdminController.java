package me.test.service.register.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import me.test.service.register.entity.User;
import me.test.service.register.model.LoginData;
import me.test.service.register.service.AdminService;

@RequestMapping("/api/admin")
@RestController
public class AdminController {

	@Autowired
	private AdminService adminService;
	
	@PostMapping(path = "/finduser", produces = {MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<User> findUser(@RequestParam("username") String username){
		LoginData loginData = new LoginData();
		loginData.setUsername(username);
		User user = adminService.findUser(loginData);
		
		if(user != null) {
			return ResponseEntity.ok(user);
		}
		else {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
		}
	}
}
