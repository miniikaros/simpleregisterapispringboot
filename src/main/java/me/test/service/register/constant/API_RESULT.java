package me.test.service.register.constant;

public enum API_RESULT {
	COMPLETED, FAIL, REJECTED
}
