package me.test.service.register.constant;

public enum API_ERROR_STATUS {
	INVALID_DATA_FORMAT, DATA_MISSING, DUPLICATE_DATA
}
