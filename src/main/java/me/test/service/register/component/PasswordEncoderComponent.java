package me.test.service.register.component;

import org.jasypt.util.password.PasswordEncryptor;
import org.jasypt.util.password.rfc2307.RFC2307SSHAPasswordEncryptor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class PasswordEncoderComponent implements PasswordEncoder  {
	
	private PasswordEncryptor passwordEncryptor;
	
	public PasswordEncoderComponent() {
		passwordEncryptor = new RFC2307SSHAPasswordEncryptor();
	}
	
	public String hashPassword(String rawPassword) {
		return passwordEncryptor.encryptPassword(rawPassword);
		
	}
	
	public boolean checkPassword(String plainPassword, String encryptedPassword) {
		return passwordEncryptor.checkPassword(plainPassword, encryptedPassword);
		
	}

	@Override
	public String encode(CharSequence rawPassword) {
		return hashPassword(rawPassword.toString());
	}

	@Override
	public boolean matches(CharSequence rawPassword, String encodedPassword) {
		return checkPassword(rawPassword.toString(), encodedPassword);
	}
}
