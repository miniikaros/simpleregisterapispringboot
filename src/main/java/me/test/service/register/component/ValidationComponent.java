package me.test.service.register.component;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

@Component
public class ValidationComponent {

	public boolean isValidName(String name) {
		return StringUtils.isNotEmpty(name) && name.length() <= NAME_MAXLENGTH;
	}
	
	public boolean isValidCurrency(String name) {
		return StringUtils.isNotEmpty(name) && name.length() <= NAME_MAXLENGTH;
	}
	
	public boolean isValidMobilePhone(String phoneNumber) {
		return StringUtils.isNumeric(phoneNumber) 
				&& phoneNumber.length() == MOBILEPHONE_MAXLENGTH 
				&& phoneNumber.charAt(0) == '0'
				&& phoneNumber.charAt(1) != '0';
	}
	
	public boolean isValidAddress(String address) {
		return StringUtils.isNotEmpty(address) 
				&& address.length() <= ADDRESS_MAXLENGTH;
	}
	
	public boolean isValidUsername(String username) {
		return StringUtils.isAlphanumeric(username) 
				&& username.length() >= USERNAME_MINLENGTH
				&& username.length() <= USERNAME_MAXLENGTH;
	}
	
	public boolean isValidPassword(String password) {
		int plen = password.length();
		if( plen < PASSWORD_MINLENGTH || plen > PASSWORD_MAXLENGTH)
			return false;
  
		int l = 0, u = 0, d = 0, s = 0; 
        for (int i = 0; i < password.length(); i++) { 
            if (password.charAt(i) >= 97 && password.charAt(i) <= 122) 
                l = 1; 
            else if (password.charAt(i) >= 65 && password.charAt(i) <= 90) 
                u = 1; 
            else if (password.charAt(i) >= 48 && password.charAt(i) <= 57) 
                d = 1; 
            else
                s = 1; 
        }
		return (l+u+d+s) > 2;
	}
	
	private static final int NAME_MAXLENGTH = 50;
	private static final int MOBILEPHONE_MAXLENGTH = 10;
	private static final int ADDRESS_MAXLENGTH = 255;
	private static final int USERNAME_MINLENGTH = 4;
	private static final int USERNAME_MAXLENGTH = 20;
	private static final int PASSWORD_MINLENGTH = 6;
	private static final int PASSWORD_MAXLENGTH = 20;

}
