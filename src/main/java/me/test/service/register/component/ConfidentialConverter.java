package me.test.service.register.component;

import org.jasypt.encryption.pbe.PooledPBEStringEncryptor;
import org.springframework.stereotype.Component;

@Component
public class ConfidentialConverter {

	private PooledPBEStringEncryptor encryptor;
	
	public ConfidentialConverter() {
		encryptor = new PooledPBEStringEncryptor();
		encryptor.setPoolSize(4);
		encryptor.setPassword(">NLS.yw^X`c5%W5S");
		encryptor.setAlgorithm("PBEWithMD5AndTripleDES");
	}
	
	public String encrypt(String data) {
		return encryptor.encrypt(data);
	}
	
	public String decrypt(String encryptedData) {
		return encryptor.decrypt(encryptedData);
	}
}
