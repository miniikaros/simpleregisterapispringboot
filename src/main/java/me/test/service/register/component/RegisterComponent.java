package me.test.service.register.component;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import me.test.service.register.constant.MEMBER_TYPE;
import me.test.service.register.constant.REGISTER_FIELD_ERROR;
import me.test.service.register.model.LoginData;
import me.test.service.register.model.RegisterData;
import me.test.service.register.model.UserData;

@Component
public class RegisterComponent {
	
	@Autowired
	private ValidationComponent validationComponent;
	
	public String getReferenceCode(LocalDate registerDate , String mobilePhone) {
		if(registerDate == null || mobilePhone == null || mobilePhone.length() < 4) {
			return null;
		}
		String last4DigitPhone = mobilePhone.substring(mobilePhone.length()-4);
		String date = registerDate.format(DateTimeFormatter.ofPattern("yyyyMMdd"));
		return String.format("%s%s", date,last4DigitPhone);
	}

	public List<REGISTER_FIELD_ERROR> validateData(RegisterData registerData) {
		List<REGISTER_FIELD_ERROR> errList = new ArrayList<>();
		if(registerData == null || registerData.getLoginData() == null || registerData.getUserData() == null) {
			errList.add(REGISTER_FIELD_ERROR.DATA_MISSING);
			return errList;
		}
		validateLoginData(registerData, errList);
		validateUserData(registerData, errList);

		return errList;
	}

	private void validateLoginData(RegisterData registerData, List<REGISTER_FIELD_ERROR> errList) {
		LoginData loginData = registerData.getLoginData();
		if(!validationComponent.isValidUsername(loginData.getUsername())) {
			errList.add(REGISTER_FIELD_ERROR.USERNAME_INVALID);
		}
		if(!validationComponent.isValidPassword(loginData.getPassword())) {
			errList.add(REGISTER_FIELD_ERROR.PASSWORD_INVALID);
		}
	}
	
	private void validateUserData(RegisterData registerData, List<REGISTER_FIELD_ERROR> errList) {
		UserData userData = registerData.getUserData();
		if(!validationComponent.isValidName(userData.getFirstname())) {
			errList.add(REGISTER_FIELD_ERROR.FIRSTNAME_INVALID);
		}
		if(!validationComponent.isValidName(userData.getLastname())) {
			errList.add(REGISTER_FIELD_ERROR.LASTNAME_INVALID);
		}
		if(!validationComponent.isValidName(userData.getMobilePhone())) {
			errList.add(REGISTER_FIELD_ERROR.MOBILEPHONE_INVALID);
		}
		if(!validationComponent.isValidAddress(userData.getAddress())) {
			errList.add(REGISTER_FIELD_ERROR.ADDRESS_INVALID);
		}
	}
	
	public MEMBER_TYPE calculateMemberType(Double salary) {
		if(salary == null || salary < 15000.00) 
			return null;
		else if(salary < 30000.00)
			return MEMBER_TYPE.SILVER;
		else if(salary <= 50000.00)
			return MEMBER_TYPE.GOLD;
		else
			return MEMBER_TYPE.PLATINUM;
	}
}
