package me.test.service.register.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

import me.test.service.register.constant.API_ERROR_STATUS;
import me.test.service.register.constant.API_RESULT;
import me.test.service.register.constant.MEMBER_TYPE;
import me.test.service.register.model.ConfidentialData;
import me.test.service.register.model.LoginData;
import me.test.service.register.model.RegisterData;
import me.test.service.register.model.RegisterResult;
import me.test.service.register.model.UserData;
import me.test.service.register.repository.UserRepository;
import me.test.service.register.component.ConfidentialConverter;
import me.test.service.register.component.PasswordEncoderComponent;

@SpringBootTest
@RunWith(MockitoJUnitRunner.class)
public class RegisterServiceTest {
	
	@Mock
	private UserRepository userRepositoryMock;
	
	@Mock
	private PasswordEncoderComponent passwordHashComponent;
	
	@Mock
	private ConfidentialConverter confidentialConverter;

	@InjectMocks
	private RegisterService registerService;
	
	private String availableUser; 
	private String duplicateUser; 
	
	private RegisterData registerData;
	
	@Before
	public void setup() {
		registerData = new RegisterData();
		registerData.setLoginData(new LoginData());
		registerData.getLoginData().setPassword("somepassword");
		registerData.setUserData(new UserData());
		registerData.getUserData().setFirstname("Meena");
		registerData.getUserData().setLastname("Meesook");
		registerData.getUserData().setAddress("123 Donmuang Bangkok 10210 Thailand");
		registerData.getUserData().setMobilePhone("0919999999");
		registerData.setConfidentialData(new ConfidentialData());
		registerData.getConfidentialData().setSalary(99999.99);
		
		availableUser = "available";
		duplicateUser = "exist";
		Mockito.when(userRepositoryMock.isUserExist(availableUser)).thenReturn(false);
		Mockito.when(userRepositoryMock.isUserExist(duplicateUser)).thenReturn(true);
	}
	
	@Test
	public void can_saveUser_userNotExist() {
		
		RegisterResult registerResult = new RegisterResult();
		registerData.getLoginData().setUsername(availableUser);
		
		registerService.saveUser(registerResult , registerData);
		
		assertEquals(API_RESULT.COMPLETED, registerResult.getResult());
		assertNull(registerResult.getError());
	}
	
	@Test
	public void cannot_saveUser_userExist() {
		
		RegisterResult registerResult = new RegisterResult();
		
		registerData.getLoginData().setUsername(duplicateUser);
		
		registerService.saveUser(registerResult , registerData);
		
		assertEquals(API_RESULT.REJECTED, registerResult.getResult());
		assertEquals(API_ERROR_STATUS.DUPLICATE_DATA, registerResult.getError());
	}
	
	

}
