package me.test.service.register.component;

import static org.junit.Assert.assertNull;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

import me.test.service.register.constant.MEMBER_TYPE;


@SpringBootTest
@RunWith(MockitoJUnitRunner.class)
public class RegisterComponentTest {
	
	@InjectMocks
	private RegisterComponent registerComponent;
	
	
	@Before
	public void setup() {
		
	}
	
	
	@Test
	public void can_getReferenceCode_inputCorrect() {
		String phone = "1234";
		LocalDate registerDate = LocalDate.parse("20190101", DateTimeFormatter.ofPattern("yyyyMMdd"));
		
		String actual = registerComponent.getReferenceCode(registerDate, phone);
		
		assertEquals("201901011234", actual);
	}
	
	@Test
	public void cannot_getReferenceCode_noRegisterDate() {
		String phone = "1234";
		LocalDate registerDate = null;
		
		String actual = registerComponent.getReferenceCode(registerDate, phone);
		
		assertNull(actual);
	}
	
	@Test
	public void cannot_getReferenceCode_noPhone() {
		String phone = null;
		LocalDate registerDate = LocalDate.parse("20190101", DateTimeFormatter.ofPattern("yyyyMMdd"));;
		
		String actual = registerComponent.getReferenceCode(registerDate, phone);
		
		assertNull(actual);
	}
	
	@Test
	public void cannot_getReferenceCode_phoneTooShort() {
		String phone = "123";
		LocalDate registerDate = LocalDate.parse("20190101", DateTimeFormatter.ofPattern("yyyyMMdd"));;
		
		String actual = registerComponent.getReferenceCode(registerDate, phone);
		
		assertNull(actual);
	}
	
	@Test
	public void can_calculateMemberType_Reject1() {
		Double salary = null;
		
		MEMBER_TYPE actual = registerComponent.calculateMemberType(salary);
		
		assertNull(actual);
	}
	
	@Test
	public void can_calculateMemberType_Reject2() {
		Double salary = 0.00;
		
		MEMBER_TYPE actual = registerComponent.calculateMemberType(salary);
		
		assertNull(actual);
	}
	
	@Test
	public void can_calculateMemberType_Reject3() {
		Double salary = 14999.99;
		
		MEMBER_TYPE actual = registerComponent.calculateMemberType(salary);
		
		assertNull(actual);
	}
	
	@Test
	public void can_calculateMemberType_SILVERMin() {
		Double salary = 15000.00;
		
		MEMBER_TYPE actual = registerComponent.calculateMemberType(salary);
		
		assertEquals(MEMBER_TYPE.SILVER, actual);
	}
	
	@Test
	public void can_calculateMemberType_SILVERMax() {
		Double salary = 29999.99;
		
		MEMBER_TYPE actual = registerComponent.calculateMemberType(salary);
		
		assertEquals(MEMBER_TYPE.SILVER, actual);
	}
	
	@Test
	public void can_calculateMemberType_GOLDMin() {
		Double salary = 30000.00;
		
		MEMBER_TYPE actual = registerComponent.calculateMemberType(salary);
		
		assertEquals(MEMBER_TYPE.GOLD, actual);
	}
	
	@Test
	public void can_calculateMemberType_GOLDMax() {
		Double salary = 50000.00;
		
		MEMBER_TYPE actual = registerComponent.calculateMemberType(salary);
		
		assertEquals(MEMBER_TYPE.GOLD, actual);
	}
	@Test
	public void can_calculateMemberType_PLATINUMMin() {
		Double salary = 50000.01;
		
		MEMBER_TYPE actual = registerComponent.calculateMemberType(salary);
		
		assertEquals(MEMBER_TYPE.PLATINUM, actual);
	}
	
	@Test
	public void can_calculateMemberType_PLATINUMMax() {
		Double salary = Double.MAX_VALUE;
		
		MEMBER_TYPE actual = registerComponent.calculateMemberType(salary);
		
		assertEquals(MEMBER_TYPE.PLATINUM, actual);
	}

}
